import React, { useState, useEffect } from 'react'
import { Typography, Grid, CircularProgress } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import axios from 'axios'
import Weather from './Weather'

const useStyles = makeStyles((theme) => ({
  title: {
    textAlign: 'center',
    marginBottom: theme.spacing(2),
  },
  progress: {
    textAlign: 'center',
  },
}))

export default function WeatherDayList() {
  const classes = useStyles()
  const [weathers, setWeathers] = useState([])
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    const loadWeather = async () => {
      setIsLoading(true)
      const { data } = await axios.get(
        'https://api.openweathermap.org/data/2.5/forecast?lat=13.944752798334894&lon=100.7473394628919&appid=41b0d242abd1a3e3100580846690bd1b'
      )
      setWeathers(data.list)
      setIsLoading(false)
    }

    loadWeather()
  }, [])
  useEffect(() => {
    console.log(weathers)
  }, [weathers])

  //return null

  return (
    <>
      <Typography variant="h4" component="h1" className={classes.title}>
        All Days
      </Typography>
      {isLoading ? (
        <div className={classes.progress}>
          <CircularProgress color="secondary"></CircularProgress>
        </div>
      ) : (
        <Grid container spacing={2}>
          {weathers.map((weather) => {
            return <Weather key={weather.dt} {...weather} />
          })}
          {}
        </Grid>
      )}
    </>
  )
}
