import React from 'react'
import {
  Grid,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Typography,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

// import cloudy from './assets/images/cloudy.png'
// import sunny from './assets/images/sunny.png'
// import rainy from './assets/images/rainy.png'
// import snowy from './assets/images/snowy.png'

const useStyles = makeStyles((theme) => ({
  media: {
    height: 200,
  },
  footer: {
    marginTop: theme.spacing(2),
  },
}))
function loadimage(icon) {
  return `http://openweathermap.org/img/wn/${icon}@2x.png`
}

export default function Weather({
  clouds,
  dt,
  dt_txt,
  main,
  pop,
  sys,
  visibility,
  weather,
  wind,
  __proto__,
}) {
  const classes = useStyles()

  const img = loadimage(weather[0].icon)
  return (
    <Grid item xs={2} sm={2} lg={2}>
      <Card style={{ backgroundColor: 'lightblue' }}>
        <CardActionArea>
          <Typography gutterBottom variant="h6" component="h2" align="center">
            {dt_txt.split(' ')[1]}
          </Typography>
          <CardMedia image={img} title={dt} className={classes.media} />
          <CardContent>
            <Typography
              gutterBottom
              variant="body2"
              color="textSecondary"
              component="p"
            >
              {weather[0].description}
            </Typography>
            <Typography
              gutterBottom
              variant="body2"
              color="textprimary"
              component="p"
            >
              Max = {Number((main.temp_max - 273.15).toFixed(2))} °C
            </Typography>
            <Typography
              gutterBottom
              variant="body2"
              color="textprimary"
              component="p"
            >
              Min = {Number((main.temp_min - 273.15).toFixed(2))} °C
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  )
}
