import React, { useState, useEffect } from 'react'
import { Typography, Grid, CircularProgress } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import axios from 'axios'
import Weather from './Weather'
import { useHistory, useRouteMatch } from 'react-router-dom'

// import Weather2 from './Weather2'

const useStyles = makeStyles((theme) => ({
  title: {
    textAlign: 'center',
    marginBottom: theme.spacing(2),
  },
  progress: {
    textAlign: 'center',
  },
}))

export default function WeatherDay2({ id }) {
  const classes = useStyles()
  const [weathers, setWeathers] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const history = useHistory()
  const { path } = useRouteMatch()
  // var today = new Date().toString().split(' ')
  var daylist = []
  // var day = today[2]
  var Max = -1000000
  var Min = 1000000
  // const mL = [
  //   'January',
  //   'February',
  //   'March',
  //   'April',
  //   'May',
  //   'June',
  //   'July',
  //   'August',
  //   'September',
  //   'October',
  //   'November',
  //   'December',
  // ]
  useEffect(() => {
    const loadWeather = async () => {
      setIsLoading(true)
      const { data } = await axios.get(
        'https://api.openweathermap.org/data/2.5/forecast?lat=13.944752798334894&lon=100.7473394628919&appid=41b0d242abd1a3e3100580846690bd1b'
      )
      setWeathers(data.list)
      setIsLoading(false)
    }

    loadWeather()
  }, [])
  useEffect(() => {
    console.log(weathers)
  }, [weathers])
  const myweather = weathers.map((weather) => weather.dt_txt.split(' ')[0])
  for (var i = 0; i < myweather.length; i++) {
    if (!daylist.includes(myweather[i])) {
      daylist.push(myweather[i])
    }
  }
  const mymax = weathers
    .filter((weather) => weather.dt_txt.split(' ')[0] === daylist[id - 1])
    .map((weather) => weather.main.temp_max)
  const mymin = weathers
    .filter((weather) => weather.dt_txt.split(' ')[0] === daylist[id - 1])
    .map((weather) => weather.main.temp_min)
  for (var j = 0; j < mymax.length; j++) {
    if (mymax[i] > Max) {
      Max = mymax[i]
    }
    if (mymin[i] < Min) {
      Max = mymin[i]
    }
  }
  const goto = () => {
    history.push(`${path}${id}`)
  }
  console.log(mymax)
  console.log(Max)
  console.log(Min)
  // return null

  return (
    <>
      <Typography
        variant="h4"
        component="h1"
        className={classes.title}
        style={{ backgroundColor: 'lightgreen' }}
        onClick={goto}
      >
        Day {id}
      </Typography>

      {isLoading ? (
        <div className={classes.progress}>
          <CircularProgress color="secondary"></CircularProgress>
        </div>
      ) : (
        <div>
          <Typography
            variant="h4"
            component="h1"
            className={classes.title}
            style={{ backgroundColor: 'lightyellow' }}
          >
            {daylist[id - 1]}
          </Typography>
          <Grid container spacing={2} justify="center">
            {weathers
              .filter(
                (weather) => weather.dt_txt.split(' ')[0] === daylist[id - 1]
              )
              .map((weather) => (
                <Weather key={weather.dt} {...weather} />
              ))}
          </Grid>
        </div>
      )}
    </>
  )
}
//แสดงทุกเวลาของวัน
