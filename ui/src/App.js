import { BrowserRouter, Route, Switch } from 'react-router-dom'
import WeatherDay from './WeatherDay'
// import WeatherList from './WeatherList'
import WeatherList2 from './WeatherList2'
import React from 'react'
// import CssBaseline from '@material-ui/core/CssBaseline'
// import Typography from '@material-ui/core/Typography'
// import Container from '@material-ui/core/Container'
import './App.css'
function App() {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route path={`/:id`}>
            <WeatherDay></WeatherDay>
          </Route>
          <Route exact path="/">
            <WeatherList2></WeatherList2>
          </Route>
          <Route path="">
            <div>Page Not Found</div>
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  )
}

export default App
