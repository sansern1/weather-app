import React from 'react'
import { Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
// import WeatherDay from './WeatherDay'
import WeatherDay2 from './WeatherDay2'
// import WeatherDay3 from './WeatherDay'
const useStyles = makeStyles((theme) => ({
  title: {
    textAlign: 'center',
    marginBottom: theme.spacing(2),
  },
  progress: {
    textAlign: 'center',
  },
}))

export default function WeatherDayList2() {
  const classes = useStyles()

  // return null

  return (
    <>
      <Typography
        variant="h4"
        component="h1"
        className={classes.title}
        style={{ backgroundColor: 'lightblue' }}
      >
        All Days
      </Typography>

      <>
        <WeatherDay2 id={1} />
        <WeatherDay2 id={2} />
        <WeatherDay2 id={3} />
        <WeatherDay2 id={4} />
        <WeatherDay2 id={5} />
      </>
    </>
  )
}
