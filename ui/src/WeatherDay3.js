import React, { useState, useEffect } from 'react'
import { Typography, Grid, CircularProgress } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import axios from 'axios'
// import Weather from './Weather'

import Weather2 from './Weather2'

const useStyles = makeStyles((theme) => ({
  title: {
    textAlign: 'center',
    marginBottom: theme.spacing(2),
  },
  progress: {
    textAlign: 'center',
  },
}))

export default function WeatherDay3({ id }) {
  const classes = useStyles()
  const [weathers, setWeathers] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  // var today = new Date().toString().split(' ')
  var daylist = []
  // var day = today[2]
  var Max = -1000000
  var Min = 1000000
  // const mL = [
  //   'January',
  //   'February',
  //   'March',
  //   'April',
  //   'May',
  //   'June',
  //   'July',
  //   'August',
  //   'September',
  //   'October',
  //   'November',
  //   'December',
  // ]
  useEffect(() => {
    const loadWeather = async () => {
      setIsLoading(true)
      const { data } = await axios.get(
        'https://api.openweathermap.org/data/2.5/forecast?lat=13.944752798334894&lon=100.7473394628919&appid=41b0d242abd1a3e3100580846690bd1b'
      )
      setWeathers(data.list)
      setIsLoading(false)
    }

    loadWeather()
  }, [])
  const myday = weathers.map((weather) => weather.dt_txt.split(' ')[0])
  for (var i = 0; i < myday.length; i++) {
    if (!daylist.includes(myday[i])) {
      daylist.push(myday[i])
    }
  }

  const mydata = weathers.filter(
    (weather) => weather.dt_txt.split(' ')[0] === daylist[id - 1]
  )

  const mymax = mydata.map((weather) => weather.main.temp_max)
  const mymin = mydata.map((weather) => weather.main.temp_min)
  for (var j = 0; j < mymax.length; j++) {
    if (mymax[j] > Max) {
      Max = mymax[j]
    }
    if (mymin[j] < Min) {
      Min = mymin[j]
    }
  }
  const myweather = mydata[0]
  console.log(myweather)
  // return null

  return (
    <>
      <Typography variant="h4" component="h1" className={classes.title}>
        Day {id}
      </Typography>

      {isLoading ? (
        <div className={classes.progress}>
          <CircularProgress color="secondary"></CircularProgress>
        </div>
      ) : (
        <>
          <Grid container spacing={2}>
            {/* {weathers
              .filter(
                (weather) => weather.dt_txt.split(' ')[0] === daylist[id - 1]
              )
              .map((weather) => (
                <Weather key={weather.dt} {...weather} />
              ))} */}
            <Weather2
              day={daylist[id] - 1}
              temp_max={Max}
              temp_min={Min}
              weather={'myweather'}
            />
          </Grid>
        </>
      )}
    </>
  )
}

//ให้แสดงอันเดียวเป็นตัวแทนของทั้งวัน
