import React from 'react'
import {
  Grid,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Typography,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

import cloudy from './assets/images/cloudy.png'
import sunny from './assets/images/sunny.png'
import rainy from './assets/images/rainy.png'
import snowy from './assets/images/snowy.png'

const useStyles = makeStyles((theme) => ({
  media: {
    height: 200,
  },
  footer: {
    marginTop: theme.spacing(2),
  },
}))
function loadimage(weather) {
  if (weather === 'Sunny') {
    return sunny
  } else if (weather === 'Clouds') {
    return cloudy
  } else if (weather === 'Rain') {
    return rainy
  } else if (weather === 'Snowy') {
    return snowy
  }
}

export default function Weather({ temp_max, temp_min, weather, day }) {
  const classes = useStyles()

  const img = loadimage(weather)
  return (
    <Grid item xs={2} sm={2} lg={2}>
      <Card>
        <CardActionArea>
          <Typography
            gutterBottom
            variant="h5"
            component="h2"
            alightItem="center"
          ></Typography>
          <CardMedia image={img} title={day} className={classes.media} />
          <CardContent>
            <Typography
              gutterBottom
              variant="body2"
              color="textSecondary"
              component="p"
            >
              {weather}
            </Typography>
            <Typography
              gutterBottom
              variant="body2"
              color="textprimary"
              component="p"
            >
              Max = {temp_max}
            </Typography>
            <Typography
              gutterBottom
              variant="body2"
              color="textprimary"
              component="p"
            >
              Min = {temp_min}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  )
}
//รับ array ที่สร้างมาเอง
